#include "library.h"

#include <d3d9.h>
#include <d3dx9.h>

#include <string>
#include <fstream>
#include <filesystem>

using namespace std::string_literals;
namespace fs = std::filesystem;

constexpr auto kLibraryName = "ScreenZone";

bool takeScreen( const char *name, int x, int y, int w, int h ) {
	static std::ofstream out;

	if ( !fs::exists( kLibraryName ) || !fs::is_directory( kLibraryName ) ) {
		if ( !fs::is_directory( kLibraryName ) )
			fs::remove_all( kLibraryName );
		if ( !fs::create_directory( kLibraryName ) ) {
			if ( !out.is_open() )
				out.open( kLibraryName + ".log"s );
			out << "Can't create directory \"" << kLibraryName << "\" for screens" << std::endl;
			return false;
		}
	}

	auto device = *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 );
	if ( !device ) {
		if ( !out.is_open() )
			out.open( kLibraryName + ".log"s );
		out << "Not found IDirect3DDevice9" << std::endl;
		return false;
	}

	auto yScr = GetSystemMetrics( SM_CYSCREEN );
	auto xScr = GetSystemMetrics( SM_CXSCREEN );
	IDirect3DSurface9 *pSurface = nullptr;
	device->CreateOffscreenPlainSurface( xScr, yScr, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, &pSurface, nullptr );
	if ( !pSurface ) {
		if ( !out.is_open() )
			out.open( kLibraryName + ".log"s );
		out << "Fail to create IDirect3DSurface9" << std::endl;
		return false;
	}

	if ( device->GetFrontBufferData( 0, pSurface ) < 0 ) {
		if ( !out.is_open() )
			out.open( kLibraryName + ".log"s );
		out << "Fail to get front buffer" << std::endl;
		return false;
	}

	POINT point{ 0, 0 };
	RECT rect{ x, y, w + x, h + y };
	ClientToScreen( *reinterpret_cast<HWND *>( 0xC97C1C ), &point );
	rect.left += point.x;
	rect.right += point.x;
	rect.top += point.y;
	rect.bottom += point.y;

	D3DXSaveSurfaceToFileA( ( kLibraryName + "/"s + name ).c_str(), D3DXIFF_JPG, pSurface, nullptr, &rect );
	pSurface->Release();
	return true;
}

bool takeScreen2( int id, int x, int y, int w, int h ) {
	return takeScreen( ( std::to_string( id ) + ".jpg" ).c_str(), x, y, w, h );
}
