#ifndef SCREENZONE_LIBRARY_H
#define SCREENZONE_LIBRARY_H

extern "C" __declspec( dllexport ) bool takeScreen( const char *name, int x, int y, int w, int h );
extern "C" __declspec( dllexport ) bool takeScreen2( int id, int x, int y, int w, int h );

#endif //SCREENZONE_LIBRARY_H
